import update from "react-addons-update";

const initialStore = {
  todayFoodList: [],
  todayTotalCals: 0
};

export const rootReducer = (store = initialStore, action) => {
  switch (action.type) {
    case "INCREMENT_CALS":
      store = {
        ...store,
        todayTotalCals: store.todayTotalCals + 1
      };
      break;
    case "ADD_FOOD":
      let index = store.todayFoodList.findIndex(
        p => p.name === action.food.name
      );

      if (index < 0) {
        store = {
          ...store,
          todayFoodList: [...store.todayFoodList, action.food]
        };
      } else {
        return store.todayFoodList.map((item,idx)=>{
          if(idx === index){
            return {
              ...item, calories: parseInt(store.todayFoodList[index].calories) +
              parseInt(action.food.calories) , quantity: parseInt(store.todayFoodList[index].quantity) +
              parseInt(action.food.quantity)
            }

          }
        })
        
        
        
 
      }
      break;
  }
  return store;
};
