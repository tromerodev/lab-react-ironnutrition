import React from "react";
import InputField from "./InputField";


export default class AddForm extends React.Component {
  constructor() {
    super();
    this.state = {
      newFood:{ name:'',calories:'',image:''},
      message:false
    };
  }


  handleChange(input, name) {
    this.state.newFood[name] = input;
    this.setState({ newFood: this.state.newFood });
  }

  handleAdd() {
    if (
      this.state.newFood.name === "" ||
      this.state.newFood.calories === "" ||
      this.state.newFood.image === ""
    ) {
      this.setState({ message: !this.state.message });
    } else {
      this.props.addFood(this.state.newFood);
      this.setState({
        newFood: {
          name: "",
          calories: "",
          image: ""
        }
      });
    }
  }

  render() {
    return (
      <React.Fragment>
        <InputField
          onChange={(input, name) => this.handleChange(input, name)}
          key="name"
          name="name"
          fasClass="fas fa-carrot"
          placeholder="Name"
        />
        <InputField
          onChange={(input, name) => this.handleChange(input, name)}
          key="calories"
          name="calories"
          fasClass="fas fa-dumbbell"
          placeholder="Calories"
        />
        <InputField
          onChange={(input, name) => this.handleChange(input, name)}
          key="image"
          name="image"
          fasClass="fas fa-images"
          placeholder="Image URL"
        />

        <div className="field">
          <p className="control">
            <button
              onClick={() => this.handleAdd()}
              className="button is-danger"
            >
              <span className="icon is-small">
                <i className="fas fa-plus-square" />
              </span>
              <span>Add</span>
            </button>
          </p>
        </div>
        {this.state.message ? (
          <div className="notification is-danger">
            Please enter all data on fields
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}
