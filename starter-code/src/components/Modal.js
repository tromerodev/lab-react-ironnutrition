import React, { Component } from "react";

import PropTypes from "prop-types";
import AddForm from "./AddForm";

export default class Modal extends Component {
  constructor() {
    super();
    this.state = {
      isActive: false,
      modalClasses: "modal"
    };
  }

  openModal() {
    this.setState({
      isActive: !this.state.isActive,
      modalClasses: this.state.modalClasses + " is-active"
    });
  }

  closeModal() {
    this.setState({
      isActive: !this.state.isActive,
      modalClasses: "modal",
    });
  }

  handleAdd(newFood){
    this.props.addFood(newFood)
    this.closeModal()
  }

  render() {
    return (
      <div className="container">
        <button
          onClick={() => this.openModal()}
          className="button is-rounded is-primary is-inverted is-large"
        >
          <span className="icon is-small" aria-label="food" role="img">
            {" "}
            🍔{" "}
          </span>
          <span> Add Food</span>
        </button>
        {/********** modal ************/}
        <div className={this.state.modalClasses}>
          <div onClick={() => this.closeModal()} className="modal-background" />
          <div className="modal-content">
            {/********** modal content ************/}
            <AddForm addFood={(newFood)=>this.handleAdd(newFood)}/>
            {/********** END -- modal content ************/}
          </div>
          <button
            onClick={() => this.closeModal()}
            className="modal-close is-large"
            aria-label="close"
          />
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  addFood: PropTypes.func
};
