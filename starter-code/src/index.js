import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bulma/css/bulma.css';
import '@fortawesome/fontawesome-free/css/all.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'
import { store } from './lib/redux/store';


ReactDOM.render(<Provider store={store}>
  <App />
  </Provider>, document.getElementById('root'));
registerServiceWorker();
